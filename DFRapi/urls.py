"""DFRapi URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include

from market import views
from market.views import GadgetListView, GadgetDetailView, GadgetListGView

urlpatterns = [
    path('admin/', admin.site.urls),
    path('api/v1/gadgets', views.get_gadgets),
    path('api/v2/gadgets', GadgetListView.as_view()),
    path('api/v2/gadget/<int:pk>', GadgetDetailView.as_view()),
    path('api/v3/gadgets', GadgetListGView.as_view()),
    path('api/v4/', include('market.urls')),
]
