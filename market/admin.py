from django.contrib import admin

# Register your models here.
from market.models import Brand, Category, Gadget

admin.site.register(Brand)
admin.site.register(Category)
admin.site.register(Gadget)
