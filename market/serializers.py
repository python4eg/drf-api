from django.contrib.auth.models import User
from rest_framework import serializers

from market.models import Gadget, Brand


class GadgetSerializer(serializers.ModelSerializer):
    brand = serializers.SlugRelatedField(
        queryset=Brand.objects.all(), slug_field='name'
    )
    author = serializers.SlugRelatedField(
        queryset=User.objects.all(), slug_field='username'
    )

    class Meta:
        model = Gadget
        fields = ('id', 'name', 'description', 'price', 'brand', 'category', 'author', 'image')