from django.db import models
from django.contrib.auth.models import User


class Brand(models.Model):
    name = models.CharField(max_length=30)

    def __str__(self):
        return str(self.name)


class Category(models.Model):
    name = models.CharField(max_length=30)

    def __str__(self):
        return str(self.name)


class Gadget(models.Model):
    name = models.CharField(max_length=30)
    description = models.TextField(blank=True)
    price = models.FloatField()
    available = models.BooleanField(default=True)
    image = models.ImageField(default='gadget.png', blank=True)
    category = models.ManyToManyField(Category)
    brand = models.ForeignKey(Brand, on_delete=models.CASCADE, default=None)
    author = models.ForeignKey(User, on_delete=models.SET_NULL, default=None, blank=True, null=True)

    class Meta:
        ordering = ['-price']

    def __str__(self):
        return f'{self.name}: {self.price}'

    def currency(self):
        return f'{self.price} ₴'
