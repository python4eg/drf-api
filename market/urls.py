from django.urls import path
from rest_framework.routers import DefaultRouter

from market.views import GadgetViewSet

router = DefaultRouter()
router.register('gadget', GadgetViewSet)

urlpatterns = router.urls

"""
Almost equal to
urlpatterns = [
    path('gadget/', GadgetViewSet.as_view({'get': 'list', 'post': 'create'})),
    path('gadget/<int:pk>/', GadgetViewSet.as_view({'get': 'retrieve',
                                                    'put': 'update',
                                                    'delete': 'destroy'}))
]
"""