from django.shortcuts import get_object_or_404
from rest_framework.decorators import api_view
from rest_framework.generics import ListCreateAPIView
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.viewsets import ModelViewSet

from market.models import Gadget
from market.serializers import GadgetSerializer


@api_view()
def get_gadgets(request):
    if request.method == 'GET':
        gadgets = Gadget.objects.all()
        resp = GadgetSerializer(gadgets, many=True)
        return Response({'data': resp.data})


class GadgetListView(APIView):
    def get(self, request):
        gadgets = Gadget.objects.all()
        resp = GadgetSerializer(gadgets, many=True)
        return Response({'gadgets': resp.data})


    def post(self, request):
        gadget = request.data.get('gadget')
        ser_model = GadgetSerializer(data=gadget)
        if ser_model.is_valid(raise_exception=True):
            saved_model = ser_model.save()
        return Response({'status': 'ok', 'message': f'{saved_model.name} successfully created'})


class GadgetDetailView(APIView):
    def get(self, request, pk):
        gadget = get_object_or_404(Gadget.objects.all(), pk=pk)
        resp = GadgetSerializer(gadget)
        return Response({'gadget': resp.data})

    def put(self, request, pk):
        gadget_to_update = get_object_or_404(Gadget.objects.all(), pk=pk)
        gadget_data = request.data.get('gadget')
        ser_model = GadgetSerializer(instance=gadget_to_update, data=gadget_data, partial=True)
        if ser_model.is_valid(raise_exception=True):
            saved_model = ser_model.save()
        return Response({'status': 'ok', 'message': f'{saved_model.name} successfully updated'})


class GadgetListGView(ListCreateAPIView):
    serializer_class = GadgetSerializer
    queryset = Gadget.objects.all()


class GadgetViewSet(ModelViewSet):
    queryset = Gadget.objects.all()
    serializer_class = GadgetSerializer

    def perform_create(self, serializer):
        author = self.request.user
        serializer.save(author=author)